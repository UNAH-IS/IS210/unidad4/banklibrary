﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankLibrary
{
    public partial class BankUIForm : Form
    {
        protected int TextBoxCount { get; set; } = 4;
        public enum TextBoxIndices { Cuenta, Nombre, Apellido, Balance }

        public BankUIForm()
        {
            InitializeComponent();
        }

        public void MostrarError(string mensaje)
        {
            MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ClearTextBoxes()
        {
            foreach(Control guiControl in Controls)
            {
                if (guiControl is TextBox)
                {
                    (guiControl as TextBox).Clear();
                }
            }
        }

        public void SetTextBoxValues(string[] values)
        {
            if (values.Length != TextBoxCount)
            {
                throw new ArgumentException($"Debe existir {TextBoxCount} cadenas en el arreglo");
            } else
            {
                int iCuenta = (int)TextBoxIndices.Cuenta;
                int iNombre1 = (int)TextBoxIndices.Nombre;
                int iApellido1 = (int)TextBoxIndices.Apellido;
                int iBalance = (int)TextBoxIndices.Balance;

                txtCuenta.Text = values[iCuenta];
                txtNombre1.Text = values[iNombre1];
                txtApellido1.Text = values[iApellido1];
                txtBalance.Text = values[iBalance];
            }
        }

        public string[] GetTextBoxValues()
        {
            return new string[]
            {
                txtCuenta.Text, txtNombre1.Text, txtApellido1.Text, txtBalance.Text
            };
        }
    }
}
