﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrearArchivo
{
    class Registro
    {
        public int Cuenta { get; set; }
        public string Nombre1 { get; set; }
        public string Apellido1 { get; set; }
        public decimal Balance { get; set; }

        public Registro() : this(0, string.Empty, string.Empty, 0M)
        {
        }

        public Registro(int cuenta, string nombre1, string apellido1, decimal balance)
        {
            this.Cuenta = cuenta;
            this.Nombre1 = nombre1;
            this.Apellido1 = apellido1;
            this.Balance = balance;
        }
    }
}
