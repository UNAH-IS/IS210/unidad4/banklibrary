﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BankLibrary;

namespace LeerArchivoAccesoSecuencial
{
    public partial class LeerArchivoAccesoSecuencial : BankUIForm
    {
        private StreamReader fileReader;

        public LeerArchivoAccesoSecuencial()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            DialogResult result;
            string nombreArchivo;

            using(var fileChooser = new OpenFileDialog()) {
                result = fileChooser.ShowDialog();
                nombreArchivo = fileChooser.FileName;
            }

            if (result == DialogResult.OK)
            {
                ClearTextBoxes();

                if (string.IsNullOrEmpty(nombreArchivo))
                {
                    MostrarError("Archivo invalido");
                }
                else
                {
                    try
                    {
                        // abre un archivo con acceso a escritura
                        var entrada = new FileStream(nombreArchivo, FileMode.Open, FileAccess.Read);
                        fileReader = new StreamReader(entrada);

                        btnAbrir.Enabled = false;
                        btnSiguiente.Enabled = true;
                    }
                    catch (IOException)
                    {
                        MostrarError("Error al abrir el archivo");
                    }
                }
            }
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                var inputRecord = fileReader.ReadLine();

                if (inputRecord != null)
                {
                    string[] inputFields = inputRecord.Split(',');
                    SetTextBoxValues(inputFields);
                } else
                {
                    fileReader.Close();
                    btnAbrir.Enabled = true;
                    btnSiguiente.Enabled = false;
                    ClearTextBoxes();

                    MessageBox.Show("No hay mas registros", string.Empty, MessageBoxButtons.OK, 
                        MessageBoxIcon.Information);
                }
            }
            catch (IOException)
            {
                MostrarError("Error al leer el archivo");
            }
        }
    }
}
