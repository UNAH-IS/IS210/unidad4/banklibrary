﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using BankLibrary;

namespace CrearArchivo
{
    public partial class CrearArchivoForm : BankUIForm
    {
        private StreamWriter fileWriter;

        public CrearArchivoForm()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            DialogResult result;
            string nombreArchivo;

            using(var dialogo = new SaveFileDialog())
            {
                dialogo.CheckFileExists = false;
                result = dialogo.ShowDialog();
                nombreArchivo = dialogo.FileName;
            }

            if (result == DialogResult.OK)
            {
                if (string.IsNullOrEmpty(nombreArchivo))
                {
                    MostrarError("Archivo invalido");
                } else
                {
                    try
                    {
                        // abre un archivo con acceso a escritura
                        var salida = new FileStream(nombreArchivo, FileMode.OpenOrCreate, FileAccess.Write);
                        fileWriter = new StreamWriter(salida);

                        btnGuardar.Enabled = false;
                    }
                    catch (IOException)
                    {
                        MostrarError("Error al abrir el archivo");
                    }
                }
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            string[] values = GetTextBoxValues();
            int iCuenta = (int)TextBoxIndices.Cuenta;
            int iNombre1 = (int)TextBoxIndices.Nombre;
            int iApellido1 = (int)TextBoxIndices.Apellido;
            int iBalance = (int)TextBoxIndices.Balance;

            if (!string.IsNullOrEmpty(values[iCuenta]))
            {
                try
                {
                    int numeroCuenta = int.Parse(values[iCuenta]);

                    if (numeroCuenta > 0)
                    {
                        var registro = new Registro(
                            numeroCuenta, 
                            values[iNombre1], 
                            values[iApellido1], 
                            decimal.Parse(values[iBalance])
                        );

                        fileWriter.WriteLine($"{registro.Cuenta},{registro.Nombre1},{registro.Apellido1},{registro.Balance}");
                    } else
                    {
                        MostrarError("Cuenta invalida");
                    }
                } catch(IOException)
                {
                    MostrarError("Error al escribir archivo");
                } catch(FormatException)
                {
                    MostrarError("Formato invalido");
                }

                ClearTextBoxes();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                fileWriter?.Close();
            } catch(IOException)
            {
                MostrarError("No se puede cerrar el archivo");
            }

            Application.Exit();
        }
    }
}
